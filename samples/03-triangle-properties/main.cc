#include <typed-geometry/tg.hh>

#include <imgui/imgui.h>

#include <glow-extras/glfw/GlfwContext.hh>
#include <glow-extras/viewer/canvas.hh>
#include <glow-extras/viewer/view.hh>

int main()
{
    glow::glfw::GlfwContext ctx;

    tg::triangle3 t = {{1, 0, -2}, {-1, 0, -2}, {0, 0, 1.5f}};

    gv::interactive([&](float) {
        ImGui::SliderFloat3("pos0", &t.pos0.x, -2, 2);
        ImGui::SliderFloat3("pos1", &t.pos1.x, -2, 2);
        ImGui::SliderFloat3("pos2", &t.pos2.x, -2, 2);

        ImGui::Separator();
        ImGui::Text("Area: %f", tg::area_of(t));
        ImGui::Text("Perimeter: %f", tg::perimeter_of(t));
        ImGui::Text("Circumradius: %f", tg::circumradius_of(t));
        ImGui::Text("Inradius: %f", tg::inradius_of(t));

        auto c = gv::canvas();
        c.add_lines(t);
        c.add_face(t);

        c.add_point(tg::centroid_of(t), tg::color3::red);
        c.add_label(tg::centroid_of(t), "centroid");

        c.add_point(tg::circumcenter_of(t), tg::color3::blue);
        c.add_lines(tg::circumcircle_of(t), tg::color3::blue);
        c.add_label(tg::circumcenter_of(t), "circumcenter");

        c.add_point(tg::incenter_of(t), tg::color3::green);
        c.add_lines(tg::incircle_of(t), tg::color3::green);
        c.add_label(tg::incenter_of(t), "incenter");
    });
}
