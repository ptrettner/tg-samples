# tg-samples

Samples for the typed geometry math library.

> :warning: this repository is an old version (and the last version without clean-core dependency). typed-geometry is being actively developed at https://github.com/project-arcana/typed-geometry. 
